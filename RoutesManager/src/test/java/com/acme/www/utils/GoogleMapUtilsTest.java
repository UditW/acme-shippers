package com.acme.www.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

/**
 * Created by uwadhwa on 9/11/17.
 */

public class GoogleMapUtilsTest {

    private GoogleMapsUtils mainClass;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ObjectMapper mapper;

    private final String mockOrigin = "Delhi";
    private final String mockDestination = "Gurgaon";
    private final String mockGoogleResponse = "{\"routes\":[{\"overview_polyline\":{\"points\":\"ABCDJD2221\"}}]}";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mainClass = new GoogleMapsUtils(restTemplate);
        ReflectionTestUtils.setField(mainClass, "mapper", mapper);
    }

    @Test
    public void testThatRestTemplateIsInvoked() {
        givenRestTemplateIsInvoked();
        whenGetPolyLineMethodIsInvoked(Mockito.anyString(), Mockito.anyString());
        thenVerifyThatGoogleServiceIsCalled();
    }


    @Test
    public void testThatCorrectOriginParamIsUsed() {
        givenRestTemplateIsInvoked();
        whenGetPolyLineMethodIsInvoked(mockOrigin, mockDestination);
        thenVerifyTheParamUsedAsOrigin();
    }

    @Test
    public void testThatCorrectDestinationParamIsUsed() {
        givenRestTemplateIsInvoked();
        whenGetPolyLineMethodIsInvoked(mockOrigin, mockDestination);
        thenVerifyParamUsedAsDestination();
    }

    @Test
    public void testThatObjectMapperIsUsedWithCorrectResponse() throws Exception {
        givenRestTemplateIsInvoked();
        whenGetPolyLineMethodIsInvoked(mockOrigin, mockDestination);
        thenVerifyThatObjectMapperIsInvokedWithCorrectResponse();
    }


    private void givenRestTemplateIsInvoked() {
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any())).thenReturn(mockGoogleResponse);
    }


    private void whenGetPolyLineMethodIsInvoked(String origin, String destination) {
        mainClass.getPolylineRoute(origin, destination);
    }


    private void thenVerifyThatGoogleServiceIsCalled() {
        Mockito.verify(restTemplate, Mockito.times(1)).getForObject(Mockito.anyString(), Mockito.any());
    }

    private void thenVerifyThatObjectMapperIsInvokedWithCorrectResponse() throws Exception {
        Mockito.verify(mapper, Mockito.times(1)).readTree(mockGoogleResponse);
    }


    private void thenVerifyTheParamUsedAsOrigin() {
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(restTemplate, Mockito.times(1)).getForObject(argumentCaptor.capture(), Mockito.any());

        Assert.assertTrue(argumentCaptor.getValue().contains(mockOrigin));
    }

    private void thenVerifyParamUsedAsDestination() {
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(restTemplate, Mockito.times(1)).getForObject(argumentCaptor.capture(), Mockito.any());

        Assert.assertTrue(argumentCaptor.getValue().contains(mockDestination));
    }

}
