$(".acme-navigation").click(function (event) {
    ele = event.target;
    var task = $(ele).data("task")
    makeServiceCall(task, "GET");
});

if ($("#msgDiv").html() == "") {
    $("#msgDiv").hide();
}

if ($("#errDiv").html() == "") {
    $("#errDiv").hide();
}

// GET DEVIATIONS METHOD
(function getDeviations() {

    $.ajax({
        url: "/deviation/get/count",
        method: "GET",
        success: function (response) {
            cnt = response["count"];
            $("#deviation-tab").html(cnt);
        }
    });
    setTimeout(getDeviations, 40000);
})();