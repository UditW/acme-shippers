$("#shipment-add").click(function () {
    freezeInputs();
    freezeSubmitButton();
    submitShipment();
});

function freezeInputs() {
    $(".form-control").attr("disabled", "disabled");
}
function freezeSubmitButton() {
    $("#shipment-add").attr("disabled", "disabled");
}

function resetInputs() {
    $(".form-control").removeAttr("disabled", "disabled");
    $(".form-control").val("");
}
function resetSubmitButton() {
    $("#shipment-add").removeAttr("disabled", "disabled");
}