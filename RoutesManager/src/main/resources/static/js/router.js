//############################ FOR SHIPMENT ###############################################
function submitShipment() {
    $.ajax({
        url: "/shipment/add",
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(getAddShipmentPostBody()),
        success: function (response) {
            $("#msgDiv").html(response["msg"]);
            $("#msgDiv").show();
            resetInputs();
            resetSubmitButton();
        }
    });

}

function getAddShipmentPostBody() {
    var text = {
        "name": $("#shipmentName").val(),
        "shipmentId": $("#shipmentId").val()
    };

    return text;
}

//############################ FOR ROUTE ###############################################


function submitroute() {
    $.ajax({
        url: "/route/add",
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(getAddRoutePostBody()),
        success: function (response) {
            $("#msgDiv").html(response["msg"]);
            $("#msgDiv").show();
            resetRoutePageInputs();
            resetRoutePageSubmitButton();
        }
    });

}

function getAddRoutePostBody() {
    var text = {
        "routeId": $("#routeId").val(),
        "truckId": $("#truckId").val(),
        "origin": $("#origin").val(),
        "destination": $("#dest").val(),
    };

    return text;
}


// ***************************** FOR TRUCK-LOAD ************************************


function submitTruckLoad() {
    $.ajax({
        url: "/load/add",
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(getAddTruckLoadPostBody()),
        success: function (response) {
            $("#msgDiv").html(response["msg"]);
            $("#msgDiv").show();
            resetTruckLoadPageInputs();
            resetTruckLoadPageSubmitButton();
        }
    });

}

function getAddTruckLoadPostBody() {
    var text = {
        "routeId": $("#routeId").val(),
        "truckId": $("#truckId").val(),
        "shipments": getTruckLoadShipments()

    };

    return text;
}

function getTruckLoadShipments() {

    /*$("tr:has(td)").each(function(){
     var tdList = $(this).find("td");
     for(var ele in tdList) {
     ele.innerHTML();
     }
     }); */

    var shipmentInventory = [];
    var temp1 = {
        "shipment": {
            "name": "",
            "shipmentId": 1
        },
        "quantity": 1
    };

    var i = 0;

    var idList = $(".shpId");
    var nameList = $(".shpNm");
    var quantityList = $(".shpQ");

    while (i < idList.size()) {
        var temp = new Object();
        if ($(quantityList[i]).find(":selected").text() == "0") {
            i++;
            continue;
        }
        else {
            temp["quantity"] = parseInt($(quantityList[i]).find(":selected").text(), 10);
        }
        temp["shipment"] = new Object();
        temp["shipment"]["name"] = $(nameList[i]).html();
        temp["shipment"]["shipmentId"] = parseInt($(idList[i]).html(), 10);

        shipmentInventory.push(temp);
        i++;
    }

    return shipmentInventory;
}

//******************************** GENERIC ******************************************

function resetMessages() {
    $("#msgDiv").html("");
    $("#msgDiv").hide();


    $("#errDiv").html("");
    $("#errDiv").hide();

}

function makeServiceCall(task, method) {

    resetMessages();
    switch (task) {
        case "add-shipment-page":
            url = "/shipment/get1";
            break;
        case "add-route-page":
            url = "/route/get/page";
            break;
        case "show-shipment":
            url = "/shipment/get";
            method = "get";
            break;
        case "show-routes":
            url = "/route/get";
            method = "get";
            break;
        case "add-truckload-page":
            url = "/load/get/page";
            method = "get"
            break;
        case "show-deviations":
            url = "/deviation/get/page";
            method = "get";
            break;
    }

    $.ajax({
        url: url,
        context: document.body,
        method: method
    }).done(function (response) {
        $("#custom-data").html(response);
    });
}