$("#truckload-add").click(function () {
    if (areAllQuantitiesZero() == true) {
        $("#errDiv").html("You must select atleast one shipment. Quantities of all shipments are zero");
        $("#errDiv").show();
        return;
    }
    else {
        $("#errDiv").html("");
        $("#errDiv").hide();
    }

    freezeInputs();
    freezeSubmitButton();
    submitTruckLoad();
});

function areAllQuantitiesZero() {
    var selectEle = $("select");
    var i = 0;
    while (i < selectEle.size()) {
        if ($(selectEle[i]).text() != "0") {
            return false;
        }
        i++;
    }
    return true;
}

function freezeInputs() {
    $(".form-control").attr("disabled", "disabled");
}
function freezeSubmitButton() {
    $("#truckload-add").attr("disabled", "disabled");
}

function resetTruckLoadPageInputs() {
    $(".form-control").removeAttr("disabled", "disabled");
    if (!$(".form-control").is("select")) {
        $(".form-control").val("");
    }
}
function resetTruckLoadPageSubmitButton() {
    $("#truckload-add").removeAttr("disabled", "disabled");
}