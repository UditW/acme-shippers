$("#route-add").click(function () {
    freezeInputs();
    freezeSubmitButton();
    submitroute();
});

function freezeInputs() {
    $(".form-control").attr("disabled", "disabled");
}
function freezeSubmitButton() {
    $("#route-add").attr("disabled", "disabled");
}

function resetRoutePageInputs() {
    $(".form-control").removeAttr("disabled", "disabled");
    $(".form-control").val("");
}
function resetRoutePageSubmitButton() {
    $("#route-add").removeAttr("disabled", "disabled");
}