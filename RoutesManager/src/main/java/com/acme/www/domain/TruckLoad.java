package com.acme.www.domain;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Entity(noClassnameStored = true)
public class TruckLoad {

    private long truckId;

    @Embedded
    private List<ShipmentInventory> shipments;

    private long routeId;

    public long getTruckId() {
        return truckId;
    }

    public void setTruckId(long truckId) {
        this.truckId = truckId;
    }

    public List<ShipmentInventory> getShipments() {
        return shipments;
    }

    public void setShipments(List<ShipmentInventory> shipments) {
        this.shipments = shipments;
    }

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }


}
