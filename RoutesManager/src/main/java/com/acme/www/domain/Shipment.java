package com.acme.www.domain;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Entity(noClassnameStored = true)
public class Shipment {

    @Property("sn")
    @Indexed(unique = true)
    private String name;

    @Property("sid")
    @Indexed(unique = true)
    private long shipmentId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(long shipmentId) {
        this.shipmentId = shipmentId;
    }
}
