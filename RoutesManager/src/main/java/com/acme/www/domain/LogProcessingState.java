package com.acme.www.domain;

/**
 * Created by uwadhwa on 9/9/17.
 */
public enum LogProcessingState {
    NEW,
    IN_PROGRESS,
    PROCESSED,
    ERROR;
}
