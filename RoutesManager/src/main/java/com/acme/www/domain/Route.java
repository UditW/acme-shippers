package com.acme.www.domain;

import com.acme.www.utils.LatLngWrapper;
import com.google.maps.model.LatLng;
import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Entity(noClassnameStored = true)
public class Route {

    @Id
    @Indexed(unique = true)
    private long routeId;

    private long truckId;

    @NotEmpty
    private String origin;

    @NotEmpty
    private String destination;

    private List<LatLngWrapper> expectedPath;

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    public long getTruckId() {
        return truckId;
    }

    public void setTruckId(long truckId) {
        this.truckId = truckId;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<LatLngWrapper> getExpectedPath() {
        return expectedPath;
    }


    public void setExpectedPath(List<LatLng> expectedPath) {
        this.expectedPath = new ArrayList<>();
        for (LatLng obj : expectedPath) {
            this.expectedPath.add(new LatLngWrapper(obj));
        }
    }
}
