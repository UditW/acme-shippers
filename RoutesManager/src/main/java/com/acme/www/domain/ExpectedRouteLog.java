package com.acme.www.domain;

import com.acme.www.utils.LatLngWrapper;
import org.mongodb.morphia.annotations.Entity;

import java.time.LocalDateTime;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Entity(noClassnameStored = true)
public class ExpectedRouteLog {

    private long truckId;

    private long routeId;

    private LocalDateTime timestamp;

    private LatLngWrapper lastDocumentedExpectedPoint;

    private int expectedPointIndex;

    public long getTruckId() {
        return truckId;
    }

    public void setTruckId(long truckId) {
        this.truckId = truckId;
    }

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LatLngWrapper getLastDocumentedExpectedPoint() {
        return lastDocumentedExpectedPoint;
    }

    public void setLastDocumentedExpectedPoint(LatLngWrapper lastDocumentedExpectedPoint) {
        this.lastDocumentedExpectedPoint = lastDocumentedExpectedPoint;
    }

    public int getExpectedPointIndex() {
        return expectedPointIndex;
    }

    public void setExpectedPointIndex(int expectedPointIndex) {
        this.expectedPointIndex = expectedPointIndex;
    }
}
