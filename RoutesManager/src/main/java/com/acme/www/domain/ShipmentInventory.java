package com.acme.www.domain;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Entity(noClassnameStored = true)
public class ShipmentInventory {

    @Embedded
    private Shipment shipment;

    private long quantity;

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
