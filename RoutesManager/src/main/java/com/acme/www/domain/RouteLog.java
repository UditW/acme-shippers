package com.acme.www.domain;

import com.acme.www.utils.LatLngWrapper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mongodb.morphia.annotations.Entity;

import java.time.LocalDateTime;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Entity(noClassnameStored = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteLog {

    private long truckId;

    private long routeId;

    private LocalDateTime timestamp;

    private LocalDateTime lastProcessedTime;

    private LatLngWrapper actualPoint;

    private LogProcessingState state;

    public long getTruckId() {
        return truckId;
    }

    public void setTruckId(long truckId) {
        this.truckId = truckId;
    }

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LatLngWrapper getActualPoint() {
        return actualPoint;
    }

    public void setActualPoint(LatLngWrapper actualPoint) {
        this.actualPoint = actualPoint;
    }

    public LogProcessingState getState() {
        return state;
    }

    public void setState(LogProcessingState state) {
        this.state = state;
    }

    public LocalDateTime getLastProcessedTime() {
        return lastProcessedTime;
    }

    public void setLastProcessedTime(LocalDateTime lastProcessedTime) {
        this.lastProcessedTime = lastProcessedTime;
    }
}
