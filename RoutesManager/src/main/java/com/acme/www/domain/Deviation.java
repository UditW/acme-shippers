package com.acme.www.domain;

import com.acme.www.utils.LatLngWrapper;
import org.mongodb.morphia.annotations.Entity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Entity(noClassnameStored = true)
public class Deviation {

    private long truckId;

    private long routeId;

    private double deviationDistance;

    private LocalDateTime timestamp;

    private LatLngWrapper lastExpectedPathPoint;

    private List<ShipmentInventory> delayedShipments;

    public long getTruckId() {
        return truckId;
    }

    public void setTruckId(long truckId) {
        this.truckId = truckId;
    }

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    public double getDeviationDistance() {
        return deviationDistance / 1000;
    }

    public void setDeviationDistance(double deviationDistance) {
        this.deviationDistance = deviationDistance;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LatLngWrapper getLastExpectedPathPoint() {
        return lastExpectedPathPoint;
    }

    public void setLastExpectedPathPoint(LatLngWrapper lastExpectedPathPoint) {
        this.lastExpectedPathPoint = lastExpectedPathPoint;
    }

    public List<ShipmentInventory> getDelayedShipments() {
        return delayedShipments;
    }

    public void setDelayedShipments(List<ShipmentInventory> delayedShipments) {
        this.delayedShipments = delayedShipments;
    }
}
