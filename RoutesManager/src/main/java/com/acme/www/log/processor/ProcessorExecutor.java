package com.acme.www.log.processor;

import com.acme.www.log.processor.utils.DeviationFinder;
import com.acme.www.log.processor.utils.DeviationLogger;
import com.acme.www.log.processor.utils.TruckMovementPersistor;
import com.acme.www.repositories.ExpectedRouteLogRepository;
import com.acme.www.repositories.RouteLogRepository;
import com.acme.www.repositories.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Component
public class ProcessorExecutor {

    @Autowired
    public ProcessorExecutor(RouteLogRepository routeLogRepository,
                             RouteRepository routeRepository,
                             ExpectedRouteLogRepository expectedRouteLogRepository,
                             DeviationFinder deviationFinder,
                             DeviationLogger deviationLogger,
                             TruckMovementPersistor movementPersistor,
                             @Value("${num.processors:1}") int numThreads) {
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);
        for (int i = 0; i < numThreads; i++) {
            RouteLogProcessor processor = new RouteLogProcessor(routeLogRepository, routeRepository,
                    expectedRouteLogRepository, deviationFinder,
                    deviationLogger, movementPersistor);
            executorService.submit(processor);
        }
    }
}
