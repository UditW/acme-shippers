package com.acme.www.log.processor.utils;

import com.acme.www.utils.LatLngWrapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by uwadhwa on 9/9/17.
 */

@Component
public class DeviationFinder {

    @Value("${permissible.deviation.metres:5000}")
    private double permissibleDeviation;

    public static void main(String argvp[]) {
        double lat = 28.636234, long1 = 77.184444;

        double currentLat = 28.638400, currentLong = 77.184412;


        //double d = distance(lat, long1, currentLat, currentLong);

    }

    public DeviationStats getDeviationStats(LatLngWrapper first, LatLngWrapper second) {
        DeviationStats deviationStats = new DeviationStats();
        deviationStats.setDistance(distance(first, second));
        if (deviationStats.getDistance() > permissibleDeviation)
            deviationStats.setDeviated(true);
        else
            deviationStats.setDeviated(false);

        return deviationStats;
    }

    private double distance(LatLngWrapper first, LatLngWrapper second) {
        double lat1 = first.lat, lng1 = first.lng;
        double lat2 = second.lat, lng2 = second.lng;

        double a = (lat1 - lat2) * distPerLat(lat1);
        double b = (lng1 - lng2) * distPerLng(lat1);
        return Math.sqrt(a * a + b * b);
    }

    private double distPerLng(double lat) {
        return 0.0003121092 * Math.pow(lat, 4)
                + 0.0101182384 * Math.pow(lat, 3)
                - 17.2385140059 * lat * lat
                + 5.5485277537 * lat + 111301.967182595;
    }

    private double distPerLat(double lat) {
        return -0.000000487305676 * Math.pow(lat, 4)
                - 0.0033668574 * Math.pow(lat, 3)
                + 0.4601181791 * lat * lat
                - 1.4558127346 * lat + 110579.25662316;
    }
}
