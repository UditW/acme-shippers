package com.acme.www.log.processor.utils;

import com.acme.www.domain.RouteLog;
import com.acme.www.repositories.DeviationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Component
public class DeviationLogger {

    private final DeviationRepository repository;

    @Autowired
    public DeviationLogger(DeviationRepository repository) {
        this.repository = repository;
    }

    public void notifyAndLogDeviation(RouteLog routeLog, DeviationStats deviationStats) {
        repository.insertDeviationEntry(routeLog, deviationStats);
    }
}
