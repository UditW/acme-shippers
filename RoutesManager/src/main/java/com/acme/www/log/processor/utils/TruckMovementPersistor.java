package com.acme.www.log.processor.utils;

import com.acme.www.domain.ExpectedRouteLog;
import com.acme.www.domain.Route;
import com.acme.www.repositories.ExpectedRouteLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Component
public class TruckMovementPersistor {


    private final ExpectedRouteLogRepository repository;

    @Autowired
    public TruckMovementPersistor(ExpectedRouteLogRepository repository) {
        this.repository = repository;
    }

    public void updateTruckMovement(ExpectedRouteLog expectedRouteLog, Route route) {
        repository.updateTruckMovement(expectedRouteLog.getRouteId(),
                route.getExpectedPath().get(expectedRouteLog.getExpectedPointIndex() + 1));

    }
}
