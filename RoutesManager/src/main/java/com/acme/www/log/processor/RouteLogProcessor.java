package com.acme.www.log.processor;

import com.acme.www.domain.ExpectedRouteLog;
import com.acme.www.domain.LogProcessingState;
import com.acme.www.domain.Route;
import com.acme.www.domain.RouteLog;
import com.acme.www.log.processor.utils.DeviationFinder;
import com.acme.www.log.processor.utils.DeviationLogger;
import com.acme.www.log.processor.utils.DeviationStats;
import com.acme.www.log.processor.utils.TruckMovementPersistor;
import com.acme.www.repositories.ExpectedRouteLogRepository;
import com.acme.www.repositories.RouteLogRepository;
import com.acme.www.repositories.RouteRepository;

/**
 * Created by uwadhwa on 9/9/17.
 */
public class RouteLogProcessor implements Runnable {

    private final RouteLogRepository repository;
    private final RouteRepository routeRepository;
    private final ExpectedRouteLogRepository expectedRouteLogRepository;
    private final DeviationFinder deviationFinder;
    private final DeviationLogger deviationLogger;
    private final TruckMovementPersistor movementPersistor;

    public RouteLogProcessor(RouteLogRepository routeLogRepository,
                             RouteRepository routeRepository,
                             ExpectedRouteLogRepository expectedRouteLogRepository,
                             DeviationFinder deviationFinder,
                             DeviationLogger deviationLogger,
                             TruckMovementPersistor movementPersistor) {
        this.repository = routeLogRepository;
        this.routeRepository = routeRepository;
        this.expectedRouteLogRepository = expectedRouteLogRepository;
        this.deviationFinder = deviationFinder;
        this.deviationLogger = deviationLogger;
        this.movementPersistor = movementPersistor;
    }

    @Override
    public void run() {
        while (true) {
            try {
                final RouteLog routeLog = repository.getUnprocessedRouteLog();

                if (routeLog == null) {
                    Thread.sleep(10000);
                    continue;
                }

                final Route route = routeRepository.getRoute(routeLog.getRouteId());

                final ExpectedRouteLog expectedRouteLog = expectedRouteLogRepository.getExpectedRouteLog(routeLog.getRouteId());

                DeviationStats fromFirstPoint = getDeviationStatsFromLastExpectedPoint(routeLog, expectedRouteLog);
                DeviationStats fromFollowingPoint = getDeviationStatsFromNextExpectedPoint(routeLog, expectedRouteLog, route);


                if (isDeviated(fromFirstPoint, fromFollowingPoint)) {
                    if (isCloserToNextPointInRoute(fromFirstPoint, fromFollowingPoint))
                        deviationLogger.notifyAndLogDeviation(routeLog, fromFollowingPoint);
                    else
                        deviationLogger.notifyAndLogDeviation(routeLog, fromFirstPoint);
                }

                if (isCloserToNextPointInRoute(fromFirstPoint, fromFollowingPoint)) {
                    movementPersistor.updateTruckMovement(expectedRouteLog, route);
                }


                repository.updateRouteLogState(routeLog, LogProcessingState.PROCESSED);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    boolean isDeviated(DeviationStats fromFirst, DeviationStats fromFollowing) {
        if (fromFirst.isDeviated() && fromFollowing.isDeviated()) {
            return true;
        }
        return false;
    }

    boolean isCloserToNextPointInRoute(DeviationStats fromFirst, DeviationStats fromFollowing) {
        if (fromFollowing.getDistance() < fromFirst.getDistance()) {
            return true;
        }
        return false;
    }

    private DeviationStats getDeviationStatsFromLastExpectedPoint(RouteLog routeLog, ExpectedRouteLog expectedRouteLog) {
        return deviationFinder.getDeviationStats(routeLog.getActualPoint(), expectedRouteLog.getLastDocumentedExpectedPoint());
    }

    private DeviationStats getDeviationStatsFromNextExpectedPoint(RouteLog routeLog, ExpectedRouteLog expectedRouteLog, Route route) {
        return deviationFinder.getDeviationStats(routeLog.getActualPoint(),
                route.getExpectedPath().get(expectedRouteLog.getExpectedPointIndex() + 1));
    }


}
