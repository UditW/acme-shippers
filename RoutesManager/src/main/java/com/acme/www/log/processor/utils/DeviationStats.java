package com.acme.www.log.processor.utils;

/**
 * Created by uwadhwa on 9/9/17.
 */
public class DeviationStats {

    private boolean deviated;

    private double distance;

    public boolean isDeviated() {
        return deviated;
    }

    public void setDeviated(boolean deviated) {
        this.deviated = deviated;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
