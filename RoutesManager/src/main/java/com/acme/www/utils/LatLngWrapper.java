package com.acme.www.utils;

import com.google.maps.model.LatLng;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Entity(noClassnameStored = true)
public class LatLngWrapper extends LatLng {
    public LatLngWrapper() {
        super(0, 0);
    }

    public LatLngWrapper(LatLng latLng){
        super(latLng.lat, latLng.lng);
    }

    public LatLngWrapper(double lat, double lng) {
        super(lat, lng);
    }
}
