package com.acme.www.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.LatLng;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Component
public class GoogleMapsUtils {

    private static final String API_KEY = "AIzaSyCjddk4GVwaUcN0pk1wpFWgqUiYSsI2CrI";
    private final ObjectMapper mapper = new ObjectMapper();
    private final RestTemplate restTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleMapsUtils.class);

    @Autowired
    public GoogleMapsUtils() {
        this.restTemplate = new RestTemplate();
    }

    // Added for tests
    public GoogleMapsUtils(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public List<LatLng> getPolylineRoute(String origin, String destination) {

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin={origin}&destination={destination}&key={API_KEY}";
        url = url.replace("{API_KEY}", API_KEY);
        url = url.replace("{origin}", origin);
        url = url.replace("{destination}", destination);

        String response = restTemplate.getForObject(url, String.class);

        try {
            JsonNode node = mapper.readTree(response);
            ArrayNode routesNode = (ArrayNode) node.at("/routes"); // /overview_polyline/points
            JsonNode routeNode = routesNode.get(0);
            TextNode polyLine = (TextNode) routeNode.at("/overview_polyline/points");
            polyLine.asText();
            return PolylineEncoding.decode(polyLine.asText());
        } catch (Exception e) {
            LOGGER.error("Error seen while fetching the Polyline.", e);
        }


        return PolylineEncoding.decode("kvkmElvvnU|@Tx@v@\\`ABjAF|Zn@?Cc\\q@{Aw@}@y@c@kBg@aD]}AD}@JeH`BuEvAcB]yDoAeAOkBNo@V{BfBcK|I}@nAeCtBuFxFmIdJmOjPaChDeBlDiAdD}ApGcDxU}@hEmAxD}[tt@yNb\\yBdEqFnJqB~DeFxM{Yrs@uKzVoCxEsEtG}BzCkHhKWh@]t@{AxEcClLuDlPwBfHaEzJoInOaBnCiF|K_Oz\\{MdZwAbDaKbUiB|CgCnDkDbEiE|FqBlDsLdXqQra@kX|m@aF|KcHtLm@pAaE~JcTxh@}Np\\kK~SyJvSmEhKuCxHoDlJsDfIcFzIaDvEiErFaKlKeLbLoErCsCjCcE~FsDrHcDtH}C|F}DtFgDtE{BrCcDpC{GjEmIjFez@vh@od@lYw^dTyl@l]oV~NmE|DwCtDsQxXiP|VcOhUcHjKuB`CgCrBqF`DmExAuGxAaXnFkDdAiEhB_DhBkQ|L{NpNyVxViCpCyCnEoD~FsAtB{AbBuArA{@x@cg@xe@wIhIeEpCwE~B}CpBwLxKqJ~IsCvCiPtReBtCiBxDsClE}D|D}EfEmCzCs@fAeBvDyE~M_JnWq@fDcAnFiA~DmCvFcAtBaBxE}IlWeCzHi@fC}@jGS~CIhF@|J@l^MfIk@tFy@tEmAlEiMz_@oJ|Yk@xBKfA?DwA|FcLn\\qGhR}BjGwBxEu@tAiD`F_AdA}@r@oApAcExCyAn@qCr@oCVkB?cD]iDO{CLsCJ}BIoBWgFq@{DWcLBiGTqDb@cCf@cDbAsEnBgCbAwEnBmBfBw@rAs@pBUjBOzC}@vIgB~Sk@lJa@tGAbCJdF[vD_@xAoA|CgIjOiHzMwFxImDlEoCfCcKjIgFlEaE|F_JbNmDhGkBrEcB|FkDhNgGjVeIjVgEzMuBnGgBlE}CxJeF~VgCvMiDbVgBvMaArIaAvFkBzGaIhReKbYsC~FsFhJyGzKqBzCuChDqFjEqOfI{BpAqCdC_BrBaBrCcKvQwBlCkBlBiDhCkFnCuNdHcDnBcCdCeBjCgAbCw@rCm@|DShEE~EIvDU~B[jBsAjEwFzKgPb\\gB`DmCnDaAz@_DrB_F`BqRjEgCb@{GrA}FrBkDhBcDxBcErDaCvCoJnLyBdC}BhBoHtEcG`GcFhFICyAdAoCbCQDaGtDgAx@eAnAuAbCe@\\e@`@kAdBUDQGSC]UkAi@cAUa@EYKWk@u@oDd@w@h@Av@\\R?dAItC|B");

    }

}
