package com.acme.www.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.google.maps.model.LatLng;

import java.io.IOException;

/**
 * Created by uwadhwa on 9/10/17.
 */
public class LatLngDeserializer extends JsonDeserializer<LatLng> {


    @Override
    public LatLng deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode jsonNode = p.getCodec().readTree(p);
        DoubleNode latNode = (DoubleNode) jsonNode.get("lat");
        DoubleNode lngNode = (DoubleNode) jsonNode.get("lng");

        return new LatLng(latNode.doubleValue(), lngNode.doubleValue());

    }
}
