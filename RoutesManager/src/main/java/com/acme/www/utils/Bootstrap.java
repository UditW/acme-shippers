package com.acme.www.utils;

import com.acme.www.domain.*;
import com.acme.www.repositories.*;
import com.google.maps.internal.PolylineEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by uwadhwa on 9/11/17.
 */
@Component
public class Bootstrap {

    private static final Logger LOGGER = LoggerFactory.getLogger(Bootstrap.class);

    private final RouteLogRepository repository;
    private final RouteRepository routeRepository;
    private final ExpectedRouteLogRepository expectedRouteLogRepository;
    private final ShipmentRepository shipmentRepository;
    private final TruckLoadRepository truckLoadRepository;

    @Autowired
    public Bootstrap(RouteLogRepository routeLogRepository,
                     ShipmentRepository shipmentRepository,
                     ExpectedRouteLogRepository expectedRouteLogRepository,
                     RouteRepository routeRepository,
                     TruckLoadRepository truckLoadRepository) {

        this.routeRepository = routeRepository;
        this.repository = routeLogRepository;
        this.expectedRouteLogRepository = expectedRouteLogRepository;
        this.shipmentRepository = shipmentRepository;
        this.truckLoadRepository = truckLoadRepository;
        init();
    }

    private void init() {
        try {
            if (shipmentRepository.getShipments() != null && shipmentRepository.getShipments().size() > 0) {
                return;
            }

            saveNewShipments();
            saveNewRoutes();
            saveNewTruckLoad();
            saveNewRouteLog();


        } catch (Exception e) {
            LOGGER.error("Error while bootstrapping.", e);
        }

    }

    private void saveNewShipments() {
        Shipment shipment = new Shipment();
        shipment.setName("TV");
        shipment.setShipmentId(122);
        shipmentRepository.saveNewShipments(shipment);


        shipment = new Shipment();
        shipment.setName("Table");
        shipment.setShipmentId(12);
        shipmentRepository.saveNewShipments(shipment);

        shipment = new Shipment();
        shipment.setName("Bike");
        shipment.setShipmentId(1222);
        shipmentRepository.saveNewShipments(shipment);

        shipment = new Shipment();
        shipment.setName("Mobile");
        shipment.setShipmentId(56);
        shipmentRepository.saveNewShipments(shipment);

        shipment = new Shipment();
        shipment.setName("Sofa");
        shipment.setShipmentId(98);
        shipmentRepository.saveNewShipments(shipment);

        shipment = new Shipment();
        shipment.setName("AC");
        shipment.setShipmentId(67);
        shipmentRepository.saveNewShipments(shipment);

        shipment = new Shipment();
        shipment.setName("Fridge");
        shipment.setShipmentId(5252);
        shipmentRepository.saveNewShipments(shipment);

    }

    private void saveNewRoutes() {
        Route route = new Route();
        route.setOrigin("Rajender Nagar");
        route.setDestination("DLF Cybercity");
        route.setRouteId(234);
        route.setTruckId(222);
        route.setExpectedPath(PolylineEncoding.decode("kvkmElvvnU|@Tx@v@\\`ABjAF|Zn@?Cc\\q@{Aw@}@y@c@kBg@aD]}AD}@JeH`BuEvAcB]yDoAeAOkBNo@V{BfBcK|I}@nAeCtBuFxFmIdJmOjPaChDeBlDiAdD}ApGcDxU}@hEmAxD}[tt@yNb\\yBdEqFnJqB~DeFxM{Yrs@uKzVoCxEsEtG}BzCkHhKWh@]t@{AxEcClLuDlPwBfHaEzJoInOaBnCiF|K_Oz\\{MdZwAbDaKbUiB|CgCnDkDbEiE|FqBlDsLdXqQra@kX|m@aF|KcHtLm@pAaE~JcTxh@}Np\\kK~SyJvSmEhKuCxHoDlJsDfIcFzIaDvEiErFaKlKeLbLoErCsCjCcE~FsDrHcDtH}C|F}DtFgDtE{BrCcDpC{GjEmIjFez@vh@od@lYw^dTyl@l]oV~NmE|DwCtDsQxXiP|VcOhUcHjKuB`CgCrBqF`DmExAuGxAaXnFkDdAiEhB_DhBkQ|L{NpNyVxViCpCyCnEoD~FsAtB{AbBuArA{@x@cg@xe@wIhIeEpCwE~B}CpBwLxKqJ~IsCvCiPtReBtCiBxDsClE}D|D}EfEmCzCs@fAeBvDyE~M_JnWq@fDcAnFiA~DmCvFcAtBaBxE}IlWeCzHi@fC}@jGS~CIhF@|J@l^MfIk@tFy@tEmAlEiMz_@oJ|Yk@xBKfA?DwA|FcLn\\qGhR}BjGwBxEu@tAiD`F_AdA}@r@oApAcExCyAn@qCr@oCVkB?cD]iDO{CLsCJ}BIoBWgFq@{DWcLBiGTqDb@cCf@cDbAsEnBgCbAwEnBmBfBw@rAs@pBUjBOzC}@vIgB~Sk@lJa@tGAbCJdF[vD_@xAoA|CgIjOiHzMwFxImDlEoCfCcKjIgFlEaE|F_JbNmDhGkBrEcB|FkDhNgGjVeIjVgEzMuBnGgBlE}CxJeF~VgCvMiDbVgBvMaArIaAvFkBzGaIhReKbYsC~FsFhJyGzKqBzCuChDqFjEqOfI{BpAqCdC_BrBaBrCcKvQwBlCkBlBiDhCkFnCuNdHcDnBcCdCeBjCgAbCw@rCm@|DShEE~EIvDU~B[jBsAjEwFzKgPb\\gB`DmCnDaAz@_DrB_F`BqRjEgCb@{GrA}FrBkDhBcDxBcErDaCvCoJnLyBdC}BhBoHtEcG`GcFhFICyAdAoCbCQDaGtDgAx@eAnAuAbCe@\\e@`@kAdBUDQGSC]UkAi@cAUa@EYKWk@u@oDd@w@h@Av@\\R?dAItC|B"));

        routeRepository.saveNewRoute(route);

        ExpectedRouteLog expectedRouteLog = new ExpectedRouteLog();
        expectedRouteLog.setTimestamp(LocalDateTime.now());
        expectedRouteLog.setExpectedPointIndex(0);
        expectedRouteLog.setTruckId(222);
        expectedRouteLog.setRouteId(234);
        expectedRouteLog.setLastDocumentedExpectedPoint(new LatLngWrapper(route.getExpectedPath().get(0).lat, route.getExpectedPath().get(0).lng));
        expectedRouteLogRepository.saveNew(expectedRouteLog);
    }

    private void saveNewTruckLoad() {
        TruckLoad truckLoad = new TruckLoad();
        truckLoad.setRouteId(234);
        truckLoad.setTruckId(222);


        truckLoad.setShipments(initShipmentInventories());
        truckLoadRepository.saveNewTruckLoad(truckLoad);
    }

    private void saveNewRouteLog() {
        RouteLog routeLog = new RouteLog();

        routeLog.setLastProcessedTime(LocalDateTime.now());
        routeLog.setTimestamp(LocalDateTime.now());
        routeLog.setState(LogProcessingState.NEW);
        routeLog.setRouteId(234);
        routeLog.setTruckId(222);
        routeLog.setActualPoint(new LatLngWrapper(12.22, 232.2));

        repository.addNewRouteLog(routeLog);


        routeLog.setLastProcessedTime(LocalDateTime.now());
        routeLog.setTimestamp(LocalDateTime.now());
        routeLog.setState(LogProcessingState.NEW);
        routeLog.setRouteId(234);
        routeLog.setTruckId(222);
        routeLog.setActualPoint(new LatLngWrapper(14.22, 231.2));

        repository.addNewRouteLog(routeLog);


    }

    private List<ShipmentInventory> initShipmentInventories() {
        List<ShipmentInventory> shipmentInventories = new ArrayList<>();

        ShipmentInventory shipmentInventory = new ShipmentInventory();
        shipmentInventory.setQuantity(3);
        Shipment shipment = new Shipment();
        shipment.setName("Sofa");
        shipment.setShipmentId(98);
        shipmentInventory.setShipment(shipment);

        shipmentInventories.add(shipmentInventory);


        shipmentInventory = new ShipmentInventory();
        shipmentInventory.setQuantity(1);
        shipment = new Shipment();
        shipment.setName("Bike");
        shipment.setShipmentId(1222);
        shipmentInventory.setShipment(shipment);

        shipmentInventories.add(shipmentInventory);


        shipmentInventory = new ShipmentInventory();
        shipmentInventory.setQuantity(5);
        shipment = new Shipment();
        shipment.setName("Table");
        shipment.setShipmentId(12);
        shipmentInventory.setShipment(shipment);

        shipmentInventories.add(shipmentInventory);


        return shipmentInventories;
    }

}
