package com.acme.www.utils;

import com.google.maps.model.LatLng;
import com.mongodb.BasicDBObject;
import org.mongodb.morphia.converters.TypeConverter;
import org.mongodb.morphia.mapping.MappedField;

/**
 * Created by uwadhwa on 9/10/17.
 */
public class LatLngTypeConverter extends TypeConverter {

    public LatLngTypeConverter() {
        super(LatLng.class);
    }

    @Override
    public Object decode(Class<?> targetClass, Object fromDBObject, MappedField optionalExtraInfo) {
        BasicDBObject object = (BasicDBObject) fromDBObject;
        double lat = (Double) object.get("lat");
        double lng = (Double) object.get("lng");


        return new LatLng(lat, lng);
    }

    @Override
    public Object encode(Object value, MappedField optionalExtraInfo) {
        LatLng latLng = (LatLng) value;
        BasicDBObject object = new BasicDBObject();
        object.put("lat", latLng.lat);
        object.put("lng", latLng.lng);

        return object;
    }
}
