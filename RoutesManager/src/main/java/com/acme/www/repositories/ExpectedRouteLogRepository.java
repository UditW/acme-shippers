package com.acme.www.repositories;

import com.acme.www.domain.ExpectedRouteLog;
import com.google.maps.model.LatLng;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Repository
public class ExpectedRouteLogRepository {

    private final Datastore datastore;

    @Autowired
    public ExpectedRouteLogRepository(Datastore datastore) {
        this.datastore = datastore;
    }

    public ExpectedRouteLog getExpectedRouteLog(long routeId) {
        Query<ExpectedRouteLog> query = datastore.createQuery(ExpectedRouteLog.class);
        query.field("routeId").equal(routeId);
        return query.get();
    }

    public void updateTruckMovement(long routeId, LatLng newExpectedPoint) {
        Query<ExpectedRouteLog> query = datastore.createQuery(ExpectedRouteLog.class);
        query.field("routeId").equal(routeId);

        UpdateOperations<ExpectedRouteLog> updateOperations = datastore.createUpdateOperations(ExpectedRouteLog.class);
        updateOperations.set("lastDocumentedExpectedPoint", newExpectedPoint);
        updateOperations.set("timestamp", LocalDateTime.now());
        updateOperations.inc("expectedPointIndex");

        datastore.findAndModify(query, updateOperations);
    }

    public void saveNew(ExpectedRouteLog expectedRouteLog) {
        datastore.save(expectedRouteLog);
    }
}
