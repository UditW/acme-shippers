package com.acme.www.repositories;

import com.acme.www.domain.Shipment;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Repository
public class ShipmentRepository {

    private final Datastore datastore;

    @Autowired
    public ShipmentRepository(Datastore datastore) {
        this.datastore = datastore;
    }

    public List<Shipment> getShipments() {
        Query<Shipment> query = datastore.createQuery(Shipment.class);

        return query.asList();
    }

    public void saveNewShipments(Shipment shipment) {
        datastore.save(shipment);
    }
}
