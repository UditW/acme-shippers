package com.acme.www.repositories;

import com.acme.www.domain.LogProcessingState;
import com.acme.www.domain.RouteLog;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Repository
public class RouteLogRepository {

    @Autowired
    private Datastore datastore;


    public RouteLog getUnprocessedRouteLog() {
        Query<RouteLog> query = datastore.createQuery(RouteLog.class);
        query.field("state").equal(LogProcessingState.NEW);

        UpdateOperations updateOperation = datastore.createUpdateOperations(RouteLog.class);
        updateOperation.set("state", LogProcessingState.IN_PROGRESS).set("lastProcessedTime", LocalDateTime.now());


        return datastore.findAndModify(query, updateOperation);

    }

    public void updateRouteLogState(RouteLog routeLog, LogProcessingState state) {
        Query<RouteLog> query = datastore.createQuery(RouteLog.class);
        query.field("routeId").equal(routeLog.getRouteId());

        UpdateOperations<RouteLog> updateOperations = datastore.createUpdateOperations(RouteLog.class);
        updateOperations.set("state", state).set("lastProcessedTime", LocalDateTime.now());

        datastore.update(query, updateOperations);
    }

    public void addNewRouteLog(RouteLog routeLog) {
        datastore.save(routeLog);
    }

}
