package com.acme.www.repositories;

import com.acme.www.domain.TruckLoad;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Repository
public class TruckLoadRepository {

    private final Datastore datastore;

    @Autowired
    public TruckLoadRepository(Datastore datastore) {
        this.datastore = datastore;
    }

    public TruckLoad getTruckLoad(long truckId, long routeId) {
        Query<TruckLoad> query = datastore.createQuery(TruckLoad.class);
        query.field("truckId").equal(truckId).field("routeId").equal(routeId);

        return query.get();
    }

    public void saveNewTruckLoad(TruckLoad truckLoad) {
        datastore.save(truckLoad);
    }
}
