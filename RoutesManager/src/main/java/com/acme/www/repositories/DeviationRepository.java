package com.acme.www.repositories;

import com.acme.www.domain.Deviation;
import com.acme.www.domain.RouteLog;
import com.acme.www.log.processor.utils.DeviationStats;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Repository
public class DeviationRepository {

    private final Datastore datastore;

    @Autowired
    public DeviationRepository(Datastore datastore) {
        this.datastore = datastore;
    }

    public void insertDeviationEntry(RouteLog routeLog, DeviationStats stats) {
        Deviation newDeviation = new Deviation();
        newDeviation.setRouteId(routeLog.getRouteId());
        newDeviation.setTimestamp(LocalDateTime.now());
        newDeviation.setTruckId(routeLog.getTruckId());
        newDeviation.setDeviationDistance(stats.getDistance());

        datastore.save(newDeviation);
    }

    public List<Deviation> getDeviations() {
        Query<Deviation> query = datastore.createQuery(Deviation.class);

        return query.asList();
    }
}
