package com.acme.www.repositories;

import com.acme.www.domain.Route;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by uwadhwa on 9/9/17.
 */
@Repository
public class RouteRepository {

    private final Datastore datastore;

    @Autowired
    public RouteRepository(Datastore datastore) {
        this.datastore = datastore;
    }

    public Route getRoute(long routeId) {
        Query<Route> query = datastore.createQuery(Route.class);
        query.field("routeId").equal(routeId);
        return query.get();
    }

    public List<Route> getRoutes() {
        Query<Route> query = datastore.createQuery(Route.class);
        return query.asList();
    }

    public void saveNewRoute(Route route) {
        datastore.save(route);
    }
}
