package com.acme.www.controllers;

import com.acme.www.domain.ShipmentInventory;
import com.acme.www.domain.TruckLoad;
import com.acme.www.repositories.ShipmentRepository;
import com.acme.www.repositories.TruckLoadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Controller
@RequestMapping("/load")
public class TruckLoadController {

    private final TruckLoadRepository repository;
    private final ShipmentRepository shipmentRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(TruckLoadController.class);

    @Autowired
    public TruckLoadController(TruckLoadRepository repository,
                               ShipmentRepository shipmentRepository) {
        this.repository = repository;
        this.shipmentRepository = shipmentRepository;
    }

    @PostMapping("/add")
    public
    @ResponseBody
    Object put(@RequestBody TruckLoad truckLoad) {
        Map<String, Object> response = new HashMap<>();

        try {
            repository.saveNewTruckLoad(truckLoad);
            response.put("msg", "TruckLoad saved successfully for given route and truck.");
        } catch (Exception e) {
            LOGGER.error("Error seen while trying to save TruckLoad.", e);
        }
        return response;
    }

    @GetMapping("/get/page")
    public String getPage(Model model) {
        model.addAttribute("shipments", shipmentRepository.getShipments());
        return "/add-truckload-page";
    }

    @GetMapping("/get")
    public
    @ResponseBody
    List<ShipmentInventory> get(@RequestParam long truckId, @RequestParam long routeId) {
        return repository.getTruckLoad(truckId, routeId).getShipments();
    }
}
