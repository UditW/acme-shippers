package com.acme.www.controllers;

import com.acme.www.domain.ExpectedRouteLog;
import com.acme.www.domain.Route;
import com.acme.www.repositories.ExpectedRouteLogRepository;
import com.acme.www.repositories.RouteRepository;
import com.acme.www.utils.GoogleMapsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Controller
@RequestMapping("/route")
public class RouteController {

    private final RouteRepository repository;
    private final GoogleMapsUtils googleMapsUtils;
    private final ExpectedRouteLogRepository expectedRouteLogRepository;

    private final Logger LOGGER = LoggerFactory.getLogger(RouteController.class);

    // mvn spring-boot:run -Drun.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"

    @Autowired
    public RouteController(RouteRepository routeRepository,
                           GoogleMapsUtils googleMapsUtils,
                           ExpectedRouteLogRepository repository) {
        this.repository = routeRepository;
        this.googleMapsUtils = googleMapsUtils;
        this.expectedRouteLogRepository = repository;
    }

    @PostMapping("/add")
    public
    @ResponseBody
    Object put(@RequestBody Route route) {
        Map<String, Object> response = new HashMap<>();

        try {
            route.setExpectedPath(googleMapsUtils.getPolylineRoute(route.getOrigin(), route.getDestination()));
            repository.saveNewRoute(route);
            expectedRouteLogRepository.saveNew(getExpectedRouteLog(route));
            response.put("msg", "Route saved successfully.");
        } catch (Exception e) {
            LOGGER.error("Error seen while saving route", e);
        }

        return response;

    }

    @GetMapping("/get")
    public String get(Model model) {
        model.addAttribute("routes", repository.getRoutes());
        return "/show-routes";
    }

    @GetMapping("/get/page")
    public String getPage(Model model) {
        return "/add-route-page";
    }

    private ExpectedRouteLog getExpectedRouteLog(Route route) {
        ExpectedRouteLog expectedRouteLog = new ExpectedRouteLog();
        expectedRouteLog.setTruckId(route.getTruckId());
        expectedRouteLog.setRouteId(route.getRouteId());
        expectedRouteLog.setExpectedPointIndex(0);
        expectedRouteLog.setLastDocumentedExpectedPoint(route.getExpectedPath().get(0));
        expectedRouteLog.setTimestamp(LocalDateTime.now());
        return expectedRouteLog;
    }
}
