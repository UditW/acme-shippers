package com.acme.www.controllers;

import com.acme.www.domain.Deviation;
import com.acme.www.domain.TruckLoad;
import com.acme.www.repositories.DeviationRepository;
import com.acme.www.repositories.TruckLoadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Controller
@RequestMapping("/deviation")
public class DeviationController {

    private final DeviationRepository repository;
    private final TruckLoadRepository truckLoadRepository;


    @Autowired
    public DeviationController(DeviationRepository repository,
                               TruckLoadRepository truckLoadRepository) {
        this.repository = repository;
        this.truckLoadRepository = truckLoadRepository;
    }


    @GetMapping("/get/count")
    public
    @ResponseBody
    Object get(Model model) {
        Map<String, Object> response = new HashMap<>();
        response.put("count", repository.getDeviations().size());
        return response;
    }

    @GetMapping("/get/page")
    public String getDeviations(Model model) {
        List<Deviation> list = repository.getDeviations();
        for (Deviation deviation : list) {
            TruckLoad truckLoad = truckLoadRepository.getTruckLoad(deviation.getTruckId(), deviation.getRouteId());
            deviation.setDelayedShipments(truckLoad.getShipments());
        }

        model.addAttribute("deviations", list);
        return "/show-deviations";
    }

    @ExceptionHandler(Exception.class)
    public String handleAllException(Exception e, Model model) {
        model.addAttribute("errmsg", "Oh Snap!!");
        return "/error";
    }
}
