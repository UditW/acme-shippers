package com.acme.www.controllers;

import com.acme.www.domain.LogProcessingState;
import com.acme.www.domain.RouteLog;
import com.acme.www.repositories.RouteLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Controller
public class TruckLocationInputController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TruckLocationInputController.class);
    private final RouteLogRepository repository;

    @Autowired
    public TruckLocationInputController(RouteLogRepository repository) {
        this.repository = repository;
    }

    @PutMapping("/truck/location")
    public
    @ResponseBody
    Object storeLocation(@RequestBody RouteLog routeLog) {

        Map<String, Object> response = new HashMap<>();

        routeLog.setState(LogProcessingState.NEW);
        routeLog.setTimestamp(LocalDateTime.now());
        routeLog.setLastProcessedTime(LocalDateTime.now());
        try {
            this.repository.addNewRouteLog(routeLog);
            response.put("msg", "Saved RouteLog successfully.");
        } catch (Exception e) {
            LOGGER.error("Error seen while trying to save RouteLog.", e);
        }

        return response;
    }
}
