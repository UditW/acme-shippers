package com.acme.www.controllers;

import com.acme.www.domain.Shipment;
import com.acme.www.repositories.ShipmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by uwadhwa on 9/10/17.
 */
@Controller
@RequestMapping("/shipment")
public class ShipmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShipmentController.class);
    private final ShipmentRepository repository;

    @Autowired
    public ShipmentController(ShipmentRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/add")
    public
    @ResponseBody
    Object put(@RequestBody Shipment shipment) {
        Map<String, Object> response = new HashMap<>();
        try {
            repository.saveNewShipments(shipment);
            response.put("msg", "Shipment saved successfully.");
        } catch (Exception e) {
            LOGGER.error("Error while trying to save shipment", e);
        }

        return response;
    }

    @GetMapping("/get")
    public String get(Model model) {
        model.addAttribute("shipments", repository.getShipments());
        return "/show-shipment";
    }

    @GetMapping("/get1")
    public String get1(Model model) {
        return "/add-service";
    }

    @ExceptionHandler(Exception.class)
    public String handleAllException(Exception e, Model model) {
        model.addAttribute("errmsg", "Oh Snap!!");
        return "/error";
    }
}
